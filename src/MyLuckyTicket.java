mport java.util.Scanner;

public class MyLuckyTicket {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print ("Please enter: ");
        String input = in.nextLine();
        in.close();
        System.out.println(input);

        if(isTicketNumberValid(input) == false){
            System.out.println("Incorrect input value.");
            return;
        };

//        System.out.println(input.substring(i,i+11));
        if(isMyTicketLucky(input)){
            System.out.println("My ticket is lucky");
        }else {
            System.out.println("My ticket is not lucky");
        };
    }


    private static boolean isTicketNumberValid(String number ) {

        try{
            Integer.parseInt(number);
        }catch(Exception e){
            return false;
        }

        if (number.length() % 2 != 0){
            return  false;
        }
        return true;
    }

    private static boolean isMyTicketLucky(String number){
        int length = number.length()/2;
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < length; i++){
            sum1 = sum1 + Integer.parseInt(number.substring(i, i+1));
        }
        for (int i = number.length(); i > length; i--){
            sum2 = sum2 + Integer.parseInt(number.substring(i-1, i));
        }
        return  sum1 == sum2;
    }

}

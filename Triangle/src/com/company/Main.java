package com.company;

import java.util.Scanner;

public class Main {

    private static int MIN = 1;
    private static int MAX = 4096;


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println ("Please enter a: ");
        String a = in.nextLine();
        if(isLengthValid(a) == false){
            System.out.println ("Incorrect a");
            return;
        }
        System.out.println ("Please enter b: ");
        String b = in.nextLine();
        if(isLengthValid(b) == false){
            System.out.println ("Incorrect b");
            return;
        }
        System.out.println ("Please enter c: ");
        String c = in.nextLine();
        if(isLengthValid(c) == false){
            System.out.println ("Incorrect c");
            return;
        }
        in.close();

        String triangleName = getTriangleName(Integer.parseInt(a), Integer.parseInt(b), Integer.parseInt(c));

        System.out.println(triangleName);
    }


    public static String getTriangleName(int a, int b, int c){
        if(a == b && b == c){
            return "Equilateral triangle";
        }

        if(a == b || b == c){
            return "Isosceles triangle";
        }


        return "Scalene trianle";

    }

    public  static  boolean isLengthValid(String input){

        try{
            Integer.parseInt(input);
        }catch (Exception e){
            return  false;
        }


        if(Integer.parseInt(input) < MIN || Integer.parseInt(input) > MAX){
            return  false;
        }

        return true;
    }


}
